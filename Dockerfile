FROM nginx:latest
COPY dist/mfe-shell/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf