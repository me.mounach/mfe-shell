import { loadRemoteModule } from '@angular-architects/module-federation';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { WebComponentWrapper, WebComponentWrapperOptions } from './utilis/web-component-wrapper';

const routes: Routes = [
  {
    path: 'contact',
    component: WebComponentWrapper,
    data: {
      remoteEntry: 'http://localhost:4300/remoteEntry.js',
      remoteName: 'vue',
      exposedModule: './web-components',
      elementName: 'vue-element'
    } as WebComponentWrapperOptions
  },
   {
    path: 'about',
    component: WebComponentWrapper,
    data: {
      remoteEntry: 'http://localhost:4400/remoteEntry.js',
      remoteName: 'react',
      exposedModule: './web-components',
      elementName: 'react-element'
    } as WebComponentWrapperOptions
  }, 
  {
    path: '',
    loadChildren: () =>
            loadRemoteModule({
                remoteEntry: 'http://localhost:4100/remoteEntry.js',
                remoteName: 'mfeClient',
                exposedModule: './Module'
            })
            .then(m => m.ClientModule)
    //loadChildren: () => import('mfeClient/Module').then(m => m.ClientModule)
  }, 
  {
    path: '',
    loadChildren: () =>
            loadRemoteModule({
                remoteEntry: 'http://localhost:4200/remoteEntry.js',
                remoteName: 'mfeAdmin',
                exposedModule: './Module'
            })
            .then(m => m.AdminModule)
    //loadChildren: () => import('mfeAdmin/Module').then(m => m.AdminModule)
  },
  { path: 'login', component: LoginComponent },
  {
    path: '',
    loadChildren: () =>
            loadRemoteModule({
                remoteEntry: 'http://localhost:4000/remoteEntry.js',
                remoteName: 'mfeHome',
                exposedModule: './Module'
            })
            .then(m => m.HomeModule)
    //loadChildren: () => import('mfeHome/Module').then(m => m.HomeModule)
  },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
