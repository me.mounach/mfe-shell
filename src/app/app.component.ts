import { Component, Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { App } from './models/app/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  constructor(
    @Inject(SESSION_STORAGE) private storage: StorageService
  ){}

  app: App = new App();

  /* -------- */
  ngOnInit() {

    if (!this.storage.get('app')) {
      this.initSessionStorage();
    }

  }

  initSessionStorage(){

    this.app.status = 'NOT AUTHORIZED';
    this.app.username = '*** NONE ***';
    this.app.email = undefined;
    this.app.error = true;
    this.app.role = undefined;

    this.storage.set('app', this.app);

  }

}
