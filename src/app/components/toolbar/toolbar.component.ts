import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { AuthService } from 'src/app/services/auth.service';
import { App } from '../../models/app/app';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  app: App = new App();

  constructor(
    @Inject(SESSION_STORAGE) private storage: StorageService,
    private authService: AuthService,
    private router: Router
    ) { }

  ngOnInit() {
  }


  get appStore() {
    return this.storage.get('app');
  }

  logout() {
    let promise = new Promise((resolve, reject) => {

      this.authService.logout()
      .subscribe((response: any) => {

        resolve(response);

        this.app.error = true; 
        this.app.email = undefined; 
        this.app.status = 'NOT AUTHORIZED';
        this.app.username = '*** NONE ***'; 
        this.app.role = undefined;
        
        this.storage.set("app", this.app);
        this.router.navigate(['/home']);

      } ,
        (err: any) => {
        reject(err);

        this.app.error = true; 
        this.app.email = undefined; 
        this.app.status = 'NOT AUTHORIZED';
        this.app.username = '*** NONE ***'; 
        this.app.role = undefined;
        
        this.storage.set("app", this.app);
        this.router.navigate(['/home']);
      }
    );
  });
  
  return promise;
  }

}
